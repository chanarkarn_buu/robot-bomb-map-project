/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.robotproject1;

import java.util.Scanner;

/**
 *
 * @author A_R_T
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(20, 20);
        Robot robot = new Robot(2, 2, 'x', map, 10);
        Bomb bomb = new Bomb(5, 5);
        map.showMap();
        map.addObj(new Tree(10, 10)); //Priority Low
        map.addObj(new Tree(9, 10));
        map.addObj(new Tree(10, 9));
        map.addObj(new Tree(11, 10));
        map.addObj(new Tree(5, 10));
        map.addObj(new Tree(15, 15));
        map.addObj(new Tree(9, 15));
        map.addObj(new Tree(15, 9));
        map.addObj(new Tree(11, 15));
        map.addObj(new Tree(5, 15));
        map.addObj(new Fuel(0, 5, 20));
        map.addObj(new Fuel(14, 12, 20));
        map.addObj(new Fuel(17, 6, 20));
        map.setBomb(bomb);
        map.setRobot(robot);  //Priority High

        while (true) {
            map.showMap();
            // W,a| N,w| E,d| S,s| q: quit
            char direction = inputDirection(kb);
            if (direction == 'q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        char direction = str.charAt(0);
        return direction;
    }
}
